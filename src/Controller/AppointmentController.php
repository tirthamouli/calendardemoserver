<?php

namespace App\Controller;

use App\Model\AppointmentModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AppointmentController extends AbstractController {

    /**
     * @Route("/appointment/get/{date}", name="get_appointments")
     */
    public function getAppointments($date){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: content-type');
        header('Content-Type: application/json; charset=UTF-8');
        $result = "allow";
        try{
            $appointment_model = new AppointmentModel();
            $result = $appointment_model->getAppointmentByDate($date);
        }catch (\Exception $e){
            $result = "ERROR";
        }
        return new JsonResponse($result);
     }
    /**
     * @Route("/appointment/getAll", name="get_all_appointments")
     */
     public function getAllAppointments(){
         header('Access-Control-Allow-Origin: *');
         header('Access-Control-Allow-Headers: content-type');
         header('Content-Type: application/json; charset=UTF-8');
         $result = "allow";
         try{
             $appointment_model = new AppointmentModel();
             $result = $appointment_model->getAllAppointments();
         }catch (\Exception $e){
             $result = "ERROR";
         }
         return new JsonResponse($result);
     }

    /**
     * @Route("/appointment/post", name="post_appointments")
     *
     */
     public function addAppointment(Request $request){
         header('Access-Control-Allow-Origin: *');
         header('Access-Control-Allow-Headers: content-type');
         header('Content-Type: application/json; charset=UTF-8');
         $result = "allow";
         try{
             if($request->getMethod()==="POST"){
                 $data = json_decode($request->getContent(), true);

                 $title = $data['title'];
                 $all_day = $data['all_day'];
                 $startDate = $data['startDate'];
                 $startTime = $data['startTime'];
                 $endDate = $data['endDate'];
                 $endTime = $data['endTime'];
                 $video_session = $data['video_session'];
                 $appointment_type = $data['appointment_type'];
                 $do_repeat = $data['do_repeat'];
                 $endRepeat = $data['endRepeat'];
                 $office_location = $data['office_location'];
                 $notes = $data['notes'];

                 $appointment_model = new AppointmentModel();
                 $result = $appointment_model->addAppointment($title, $all_day, $startDate, $startTime, $endDate, $endTime, $video_session, $appointment_type, $do_repeat, $endRepeat, $office_location, $notes);
             }
         }catch (\Exception $e){
             $result = "ERROR";
         }
         return new JsonResponse($result);
     }
}