<?php
namespace App\Model;

class AppointmentModel{
    private $link;

    public function __construct(){
        \mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $this->link = new \mysqli("localhost","tirth","manjoo#53", "calendar");
    }

    /**
     * @param $title
     * @param $all_day
     * @param $startDate
     * @param $startTime
     * @param $endDate
     * @param $endTime
     * @param $video_session
     * @param $appointment_type
     * @param $do_repeat
     * @param $endRepeat
     * @param $office_location
     * @param $notes
     * @return bool|int|\mysqli_result
     */
    public function addAppointment($title, $all_day, $startDate, $startTime, $endDate, $endTime, $video_session, $appointment_type, $do_repeat, $endRepeat, $office_location, $notes){
        try{
            $query = "insert into appointments(title, all_day, startDate, startTime, endDate, endTime, video_session, appointment_type,
                                               do_repeat, endRepeat, office_location, notes)
                                        values('$title', $all_day, '$startDate', '$startTime', '$endDate', '$endTime', $video_session, '$appointment_type', 
                                               '$do_repeat', '$endRepeat', '$office_location', '$notes')";
            $result = $this->link->query($query);
            return $result;
        }catch(\Exception $e){
            return -1;
        }

    }

    /**
     * @param $date
     * @return mixed
     */
    public function getAppointmentByDate($date){
        $query = "select * from appointments where startDate = '$date' order by startDate";
        $result = $this->link->query($query);
        return $result->fetch_all(true);
    }

    /**
     * @return mixed
     */
    public function getAllAppointments(){
        $query = "select * from appointments order by startDate";
        $result = $this->link->query($query);
        return $result->fetch_all(true);
    }

    public function __destruct()
    {
        $this->link->close();
    }

}